# Better Code Macro for Confluence

This is a better Code Macro for Confluence Cloud. It was designed, from the ground up, to support a
Cloud environment and all of the great integrations that you would expect from a cloud environment.
