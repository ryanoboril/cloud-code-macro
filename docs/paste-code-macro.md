# New and improved Code Block macro for Confluence Cloud

The Better Code Block macro is a drop in replacement for the default Confluence Code Block macro. It
has all of the same features plus some more.

## Better syntax highlighting

The default code block macro supports a meager 19 languages, whereas the Better Code Macro supports
149 unique languages! It contains support for all of the common languages that you might expect plus
other more niche languages too.

## Better theme support

The default code block macro supports only 8 themes, wheras the Better Code Macro supports 71 unique
themes. Even beter than that, the Better Code Macro does not suffer from [a bug in the default code
block macro][1] that prevents you from using more than one theme on a page.

## Copy to clipboard support

There is rudimentary copy to clipboard support in the default code block macro but most users don't
even know that it exists. The Better Code Macro elevates copy to clipboard support to first class
support; enabling a simple button press to copy the entire paste to your clipboard.

## Almost total backwards compatibility

The Better Code Macro is nearly fully compatible with the default code block macro.
The only feature that the Better Code Macro does not support is 'line numbers'; and that was a
deliberate choice. If you need line number support then you should use a Bitbucket snippert or a
Github gist.

 [1]: https://jira.atlassian.com/browse/CONF-28582
