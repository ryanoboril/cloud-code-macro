# Bitbucket Snippet Code Macro

You can embed bitbucket snippets dircectly into your Confluence pages. You achieve that by pasting the link to that
snippet directly into a page. For example, if you were to copy this Bitbucket Snippet url:

> https://bitbucket.org/snippets/robertmassaioli/LReGb

And then paste it into a confluence page (after the Better Code Macro add-on was installed) then it
would be automatically converted into a Bitbucket Snippet Code Macro and the contents of that
snippet would be visible on your page.

Note: Currently private snippets are not supported.
