# Frequently Asked Questions

#### Why is the Better Code Macro for Confluence Cloud actually better?

The better code macro for Confluence Cloud has full feature compatability with the default Code
Block macro; making it at-least as good as the default macro. However, the features that make it
even better are:

 - Many more supported languages!
 - Many more supported themes!
 - Top level copy-to-clipboard support
 - Bitbucket Snippet support (with autocomplete)
 - Github Gist support (with autocomplete)

And, to top it all off, it is completely free. Forever.

#### Will this add-on be free forever?

Yes, this add-on will be free forever. It is run by Atlassian on our internal servers. It is also
developed by Atlassians. We are giving this away for free to our awesome customers; we love our
customers.

#### Do you store any of my data?

No. The Better Code Macro does not store any of your data and, since the Better Code Macro is hosted
on Atlassian owned infrastructure, your data never leaves the Atlassian ecosystem. The Better Code
Macro keeps your data secure by not storing it.

#### Is there copy-to-clipboard support for Bitbucket snippets and GitHub gists?

No; and there will likely never be until Bitbucket and GitHub implement it themselves. The idea here
is that we want to give you the default Bitbucket and GitHub embed experience (whatever that may
be). We are not going to try and extend their experience. If you want them to support copy to
clipboard then you will have to contact them with your request. 

Until then you may have to copy the text manually.
