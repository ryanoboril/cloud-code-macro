# Better Code Macro for Confluence Cloud

The current code macro for Confluence Cloud is lacking some features so we have made you a better
code block macro from scratch that has the following extra features:

 - Full raw paste backwards compatability (except for line number support)
 - More supported programming and markup languages!
 - Many many more supported themes!
 - Bitbucket Snippet Support
 - GitHub Gist Support
 - Copy-to-clipboard support!

The Better Code Macro for Confluence Cloud is a dramatic improvement over the existing Code Block
macro and it could be a powerful tool in your confluence page writing arsenal.
