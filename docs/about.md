# About

The Better Code Macro for Confluence Cloud was written by Robert Massaioli for ShipIt 34. It was
designed to be an Atlassian Connect add-on that made the code block macro a better experience for
Confluence Cloud customers. It is currently free and will be free forever.
