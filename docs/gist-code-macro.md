# GitHub Gist Code Macro

The gist code macro will embed a github gist directly into your Confluence pages. To use the macro
simply copy the url to a github gist, like this one:

> https://gist.github.com/robertmassaioli/18f8a5465eae7b708d00

If you paste that link directly into a Confluence page (once the Better Code Macro add-on is
installed) then it will automatically convert into a Gist Code Block macro. Give it a try now!
